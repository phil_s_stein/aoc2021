package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {

	if len(os.Args) != 2 {
		log.Fatal("bad args")
	}

	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	field := NewField()

	s := bufio.NewScanner(f)
	for s.Scan() {
		tkns := strings.Split(s.Text(), " ")
		p1 := strings.Split(tkns[0], ",")
		p2 := strings.Split(tkns[2], ",")

		// only add horizontal or vertical lines.
		if p1[0] == p2[0] || p1[1] == p2[1] {
			field.AddLine(
				str2int(p1[0]), str2int(p1[1]),
				str2int(p2[0]), str2int(p2[1]),
			)
		}
	}

	if err := s.Err(); err != nil {
		log.Fatal(err)
	}

	// fmt.Printf("Field:\n%s\n", field)
	fmt.Printf("Overlap: %d\n", field.OverlapCount())
}
