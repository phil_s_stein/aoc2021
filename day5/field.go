package main

import "fmt"

type Point struct {
	X int
	Y int
}

type Field struct {
	Area map[Point]int
	Size int
}

func NewField() *Field {
	return &Field{
		Area: make(map[Point]int),
		Size: 0,
	}
}

func (p Point) String() string {
	return fmt.Sprintf("%d,%d", p.X, p.Y)
}

func (f *Field) AddLine(x1, y1, x2, y2 int) {

	x, xe := x1, x2
	y, ye := y1, y2

	if x1 > f.Size {
		f.Size = x1
	}
	if x2 > f.Size {
		f.Size = x2
	}
	if y1 > f.Size {
		f.Size = y1
	}
	if y2 > f.Size {
		f.Size = y2
	}

	for {

		f.Area[Point{x, y}]++

		if x == xe && y == ye {
			break
		}

		// figure the incrment/decrement
		if x1 < x2 {
			x++
		} else if x1 > x2 {
			x--
		}

		if y1 < y2 {
			y++
		} else if y1 > y2 {
			y--
		}
	}
}

func (f *Field) OverlapCount() int {

	c := 0
	for _, s := range f.Area {
		if s >= 2 {
			c++
		}
	}

	return c
}

func (f *Field) String() string {

	s := fmt.Sprintf("Area: %dx%d\n", f.Size, f.Size)

	for y := 0; y <= f.Size; y++ {
		for x := 0; x <= f.Size; x++ {
			p := Point{x, y}
			if f.Area[p] == 0 {
				s += fmt.Sprintf(".")
			} else {
				s += fmt.Sprintf("%d", f.Area[Point{x, y}])
			}
		}
		s += fmt.Sprintf("\n")
	}

	return s
}
