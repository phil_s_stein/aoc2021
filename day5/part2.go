package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strings"
)

func main() {

	if len(os.Args) != 2 {
		log.Fatal("bad args")
	}

	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	field := NewField()

	s := bufio.NewScanner(f)
	for s.Scan() {
		tkns := strings.Split(s.Text(), " ")
		p1 := strings.Split(tkns[0], ",")
		p2 := strings.Split(tkns[2], ",")

		x1, y1, x2, y2 := str2int(p1[0]), str2int(p1[1]), str2int(p2[0]), str2int(p2[1])

		diag := math.Abs(float64(x1-x2)) == math.Abs(float64(y1-y2))

		if x1 == x2 || y1 == y2 || diag {
			field.AddLine(x1, y1, x2, y2)
		}
	}

	if err := s.Err(); err != nil {
		log.Fatal(err)
	}

	// fmt.Printf("Field:\n%s\n", field)
	fmt.Printf("Overlap: %d\n", field.OverlapCount())
}
