package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
)

func ReadFile(path string) []string {

	f, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	out := []string{}

	s := bufio.NewScanner(f)
	for s.Scan() {
		out = append(out, s.Text())
	}

	if err := s.Err(); err != nil {
		log.Fatal(err)
	}

	return out
}

func str2int(str string) int {

	s, err := strconv.Atoi(str)
	if err != nil {
		log.Fatal(err)
	}

	return s
}
