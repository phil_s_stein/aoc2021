package main

import (
	"fmt"
)

func main() {

	lines := ReadFile("./input")

	inc := 0

	for i := 0; i < len(lines)-1; i++ {

		if str2int(lines[i]) < str2int(lines[i+1]) {
			inc++
		}
	}

	fmt.Println(inc)
}
