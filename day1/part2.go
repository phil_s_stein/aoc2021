package main

import "fmt"

func SumWindow(d []string, start, size int) int {

	s := 0
	for _, x := range d[start : start+size] {
		s += str2int(x)
	}

	return s
}

func main() {

	lines := ReadFile("./input")

	inc := 0
	winSize := 3

	for i := 0; i < len(lines)-winSize; i++ {

		if SumWindow(lines, i, winSize) < SumWindow(lines, i+1, winSize) {
			inc++
		}
	}

	fmt.Println(inc)
}
