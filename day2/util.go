package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

const (
	FWD = "forward"
	DWN = "down"
	UP  = "up"
)

type Command struct {
	Value int
	Cmd   string
}

type Sub struct {
	Depth int
	Pos   int
	Aim   int
}

func (s *Sub) DoNaiveCmd(c *Command) error {

	switch c.Cmd {
	case FWD:
		s.Pos += c.Value
	case DWN:
		s.Depth += c.Value
	case UP:
		s.Depth -= c.Value
	default:
		return fmt.Errorf("Bad cmd: %s", c.Cmd)
	}

	return nil
}

func (s *Sub) DoCmd(c *Command) error {

	switch c.Cmd {
	case DWN:
		s.Aim += c.Value
	case UP:
		s.Aim -= c.Value
	case FWD:
		s.Pos += c.Value
		s.Depth += s.Aim * c.Value
	default:
		return fmt.Errorf("Bad cmd: %s", c.Cmd)
	}

	return nil
}

func (s *Sub) String() string {
	return fmt.Sprintf("{Position: %d; Depth %d; Aim: %d}", s.Pos, s.Depth, s.Aim)
}

func ReadInput(path string) []*Command {

	f, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	out := []*Command{}

	s := bufio.NewScanner(f)
	for s.Scan() {
		tkns := strings.Split(s.Text(), " ")
		out = append(out, &Command{
			Cmd:   tkns[0],
			Value: str2int(tkns[1]),
		})
	}

	if err := s.Err(); err != nil {
		log.Fatal(err)
	}

	return out
}

func str2int(str string) int {

	s, err := strconv.Atoi(str)
	if err != nil {
		log.Fatal(err)
	}

	return s
}
