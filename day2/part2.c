#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LINE_LEN 255

typedef enum {
    UP=0,
    DWN,
    FWD
} CmdType;

typedef struct  Cmd {
    int value;
    CmdType cmd;
} Cmd;

typedef struct Cmds {
    int len;
    Cmd *cmd;
} Cmds;

Cmds *ReadInput(char *path) {

    FILE *fd = fopen(path, "r");

    if (!fd) {
        perror(path);
        exit(EXIT_FAILURE);
    }

    char buf[LINE_LEN] = {0};
    int cmdCnt = 0;

    while (fgets(buf, LINE_LEN, fd)) {
        cmdCnt++;
    }

    fclose(fd);
    
    Cmds *cmds = malloc(sizeof(struct Cmds));
    cmds->len = cmdCnt;
    cmds->cmd = malloc(cmdCnt * sizeof(struct Cmd));

    int i = 0;

    fd = fopen(path, "r"); 

    while (fgets(buf, LINE_LEN, fd)) {
       char c[LINE_LEN] = {0};
       int value = 0;

       sscanf(buf, "%s %d", (char*)&c, &value);

       if (strcmp(c, "up") == 0) {
           cmds->cmd[i].cmd = UP;
       } else if(strcmp(c, "down") == 0) {
           cmds->cmd[i].cmd = DWN;
       } else if (strcmp(c, "forward") == 0) {
           cmds->cmd[i].cmd = FWD;
       } else {
           printf("bad cmd %s line %d\n", c, i);
           exit(EXIT_FAILURE);
       }

       cmds->cmd[i].value = value;

       i++;
    }

    fclose(fd);

    return cmds;
}

void freeCmds(Cmds *c) {
    free(c->cmd);
    free(c);
}

int main(int argc, char **argv) {

    if (argc != 2) {
        printf("bad args\n");
        exit(EXIT_FAILURE);
    }

    Cmds *cmds = ReadInput(argv[1]); 

    int aim=0, pos=0, depth=0;

    for (int i=0; i<cmds->len; i++) {
        
        switch(cmds->cmd[i].cmd) {
            case UP:
                aim -= cmds->cmd[i].value;
                break;
            case DWN:
                aim += cmds->cmd[i].value;
                break;
            case FWD:
                pos += cmds->cmd[i].value;
                depth += aim * cmds->cmd[i].value;
                break;
            default:
                printf("bad cmd\n");
                exit(EXIT_FAILURE);
        }
    }

    freeCmds(cmds);

    printf("Answer: %d\n", pos*depth);

    return EXIT_SUCCESS;
}
