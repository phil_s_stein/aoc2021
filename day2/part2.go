package main

import (
	"fmt"
	"log"
	"os"
)

func main() {

	if len(os.Args) != 2 {
		log.Fatal("bad args")
	}

	cmds := ReadInput(os.Args[1])

	s := &Sub{}
	for _, c := range cmds {

		err := s.DoCmd(c)
		if err != nil {
			log.Fatal(err)
		}
	}

	fmt.Printf("Sub: %s. Answer: %d\n", s, s.Depth*s.Pos)
}
