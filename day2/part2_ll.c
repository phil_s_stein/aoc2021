#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LINE_LEN 255

typedef enum {
    UP=0,
    DWN,
    FWD
} CmdType;

typedef struct  Cmd {
    int value;
    CmdType cmd;
    struct Cmd *next;
} Cmd;

Cmd *ReadInput(char *path) {

    FILE *fd = fopen(path, "r");

    if (!fd) {
        perror(path);
        exit(EXIT_FAILURE);
    }

    char buf[LINE_LEN] = {0};
    Cmd *head = NULL;
    Cmd **cur = &head;

    while (fgets(buf, LINE_LEN, fd)) {
        char c[LINE_LEN] = {0};
        int value = 0;

        sscanf(buf, "%s %d", (char*)&c, &value);

        (*cur) = malloc(sizeof(Cmd));
        (*cur)->next = NULL;

        if (strcmp(c, "up") == 0) {
            (*cur)->cmd = UP;
        } else if(strcmp(c, "down") == 0) {
            (*cur)->cmd = DWN;
        } else if (strcmp(c, "forward") == 0) {
            (*cur)->cmd = FWD;
        } else {
            printf("bad cmd %s\n", c);
            exit(EXIT_FAILURE);
        }

        (*cur)->value = value;

        // set cur to next node addresss
        cur = &((*cur)->next); 
    }

    fclose(fd);
    
    return head;
}

void freeCmds(Cmd *c) {
    Cmd *cur = c;
    Cmd *next = NULL;

    while (cur) {
        next = cur->next;
        free(cur);
        cur = next;
    }
}

int main(int argc, char **argv) {

    if (argc != 2) {
        printf("bad args\n");
        exit(EXIT_FAILURE);
    }

    Cmd *head = ReadInput(argv[1]); 
    Cmd *c = head;

    int aim=0, pos=0, depth=0;

    while (c) {
        
        switch(c->cmd) {
            case UP:
                aim -= c->value;
                break;
            case DWN:
                aim += c->value;
                break;
            case FWD:
                pos += c->value;
                depth += aim * c->value;
                break;
            default:
                printf("bad cmd\n");
                exit(EXIT_FAILURE);
        }

        c = c->next;
    }

    freeCmds(head);

    printf("Answer: %d\n", pos*depth);

    return EXIT_SUCCESS;
}
