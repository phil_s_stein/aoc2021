package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {

	if len(os.Args) != 3 {
		log.Fatal("bad args")
	}

	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	count := str2int(os.Args[2])
	insMap := make(map[string]string)
	input := ""
	scan := bufio.NewScanner(f)

	for scan.Scan() {

		line := scan.Text()
		if len(line) == 0 {
			continue
		}

		if input == "" {
			input = line
			continue
		}

		tkns := strings.Split(line, " ")
		insMap[tkns[0]] = tkns[2]
	}

	fmt.Printf("input: %s\n", input)
	fmt.Printf("count: %d\n", count)
	fmt.Printf("map: %v\n", insMap)

	end := string(input[len(input)-1])
	result := make(map[string]int)
	for i := 0; i < len(input)-1; i++ {
		result[input[i:i+2]] = 1
	}

	for c := 0; c < count; c++ {
		tmp := make(map[string]int)
		for k := range result {
			if val, ok := insMap[k]; ok {
				p := string(k[0]) + val
				tmp[p]++
			} else {
				log.Fatalf("incomplete input map missing %s", k)
			}
		}
		tmp
		result = tmp
	}

	fmt.Printf("%v\n", result)

	// most, least := getCount(result)
	// fmt.Printf("result: %d\n", most-least)
}

func getCount(str string) (int, int) {
	cnt := make(map[rune]int)
	for _, c := range str {
		cnt[c]++
	}
	most, least := 0, 9223372036854775807
	for _, c := range cnt {
		if c > most {
			most = c
		}
		if c < least {
			least = c
		}
	}
	return most, least
}

func str2int(str string) int {

	s, err := strconv.Atoi(str)
	if err != nil {
		log.Fatal(err)
	}

	return s
}
