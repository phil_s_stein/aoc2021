package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {

	if len(os.Args) != 3 {
		log.Fatal("bad args. want: file iterations")
	}

	file, iterations := os.Args[1], os.Args[2]

	f, err := os.Open(file)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	fish := [9]int64{}

	s := bufio.NewScanner(f)
	for s.Scan() {
		tkns := strings.Split(s.Text(), ",")

		for _, t := range tkns {
			fish[str2int(t)]++
		}
	}

	if err := s.Err(); err != nil {
		log.Fatal(err)
	}

	for i := 0; i < str2int(iterations); i++ {

		spawn := fish[0]

		for f := 1; f < len(fish); f++ {
			fish[f-1] = fish[f]
		}

		fish[len(fish)-1] = spawn
		fish[len(fish)-3] += spawn
	}

	fmt.Printf("Fish: %v\n", fish)
	fmt.Printf("Fish count: %d\n", fishCount(fish))
}

func fishCount(fish [9]int64) int {
	s := 0
	for _, f := range fish {
		s += int(f)
	}
	return s
}

func str2int(str string) int {

	s, err := strconv.Atoi(str)
	if err != nil {
		log.Fatal(err)
	}

	return s
}
