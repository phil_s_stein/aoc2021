package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
)

var (
	ValidChars = map[string]string{
		"(": ")",
		"[": "]",
		"<": ">",
		"{": "}",
	}
	IncScore = map[string]int{
		"(": 1,
		"[": 2,
		"{": 3,
		"<": 4,
	}
	CorScore = map[string]int{
		")": 3,
		"]": 57,
		"}": 1197,
		">": 25137,
	}
)

func main() {

	if len(os.Args) != 2 {
		log.Fatal("bad args")
	}

	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	score := 0
	incScores := []int{}

	scan := bufio.NewScanner(f)
	for scan.Scan() {

		s := &Stack{}
		line := scan.Text()
		corrupt := false

		for _, t := range line {
			cur := string(t)
			if _, ok := ValidChars[cur]; ok {
				s.Push(cur) // start of block
			} else {
				if cur != ValidChars[s.Pop()] {
					score += CorScore[cur]
					corrupt = true
					break
				}
			}
		}

		if len(s.data) != 0 && !corrupt {
			// missing ends. score the line as incomplete.
			incS := 0
			for {
				t := s.Pop()
				if t == "" {
					break
				}
				incS = (incS * 5) + IncScore[t]
			}
			incScores = append(incScores, incS)
		}
	}

	if err := scan.Err(); err != nil {
		log.Fatal(err)
	}

	sort.Ints(incScores)

	fmt.Printf("corrupt score: %d\n", score)
	fmt.Printf("incomplete score: %d\n", incScores[len(incScores)/2])
}

type Stack struct {
	data []string
}

func (s *Stack) Push(str string) {
	s.data = append(s.data, str)
}

func (s *Stack) Pop() string {
	l := len(s.data)
	if l == 0 {
		return ""
	}

	res := s.data[l-1]
	s.data = s.data[:l-1]

	return res
}
