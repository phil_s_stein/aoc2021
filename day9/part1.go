package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
)

func main() {

	if len(os.Args) != 2 {
		log.Fatal("bad args")
	}

	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	size := 0
	var grid Grid

	s := bufio.NewScanner(f)
	for s.Scan() {

		t := s.Text()

		if size == 0 {
			size = len(t)
		}

		row := make([]int, len(t))

		for i := 0; i < len(t); i++ {
			row[i] = int(t[i] - '0') // rune to int trickery
		}

		grid = append(grid, row)
	}

	if err := s.Err(); err != nil {
		log.Fatal(err)
	}

	// fmt.Printf("grid: \n%s", grid)

	risk := 0
	basins := []int{}

	// fine local mins
	for i := range grid {
		for j := range grid[i] {
			localMin := true
			for _, nbr := range grid.GetNbrValues(i, j) {
				if nbr <= grid[i][j] {
					localMin = false
					break
				}
			}
			if localMin {
				risk += grid[i][j] + 1

				seen := make([][]bool, len(grid))
				for r := range seen {
					seen[r] = make([]bool, len(grid[i]))
				}

				bs := grid.GetBasinSize(i, j, &seen)
				basins = append(basins, bs)
			}
		}
	}

	fmt.Printf("risk: %d\n", risk)
	sort.Ints(basins)
	fmt.Printf("basin sizes: %v\n", basins)
	fmt.Printf("answer: %d\n", basins[len(basins)-1]*basins[len(basins)-2]*basins[len(basins)-3])

}

type Grid [][]int

func NewGrid(size int) Grid {
	g := make(Grid, size)
	for i := range g {
		g[i] = make([]int, size)
	}
	return g
}

func (g Grid) GetNbrValues(i, j int) []int {

	is, js := g.GetNrbIndexes(i, j)
	values := []int{}
	for index := range is {
		values = append(values, g[is[index]][js[index]])
	}

	return values
}

func (g Grid) GetNrbIndexes(i, j int) ([]int, []int) {

	// corners
	if i == 0 && j == 0 {
		return []int{0, 1}, []int{1, 0}
	}
	if i == 0 && j == len(g[i])-1 {
		return []int{0, 1}, []int{j - 1, j}
	}
	if i == len(g)-1 && j == 0 {
		return []int{i, i - 1}, []int{j + 1, j}
	}
	if i == len(g)-1 && j == len(g[i])-1 {
		return []int{i, i - 1}, []int{j - 1, j}
	}

	// top/bottom/side row
	if j == 0 {
		return []int{i - 1, i + 1, i}, []int{j, j, j + 1}
	}
	if j == len(g[i])-1 {
		return []int{i - 1, i + 1, i}, []int{j, j, j - 1}
	}
	if i == 0 {
		return []int{i, i, i + 1}, []int{j - 1, j + 1, j}
	}
	if i == len(g)-1 {
		return []int{i, i, i - 1}, []int{j - 1, j + 1, j}
	}

	// middle somewhere
	return []int{i, i, i - 1, i + 1}, []int{j + 1, j - 1, j, j}
}

func (g Grid) GetBasinSize(i, j int, seen *[][]bool) int {

	s := 1 // count ourselves.
	is, js := g.GetNrbIndexes(i, j)

	for index := range is {
		iis, jjs := is[index], js[index]
		nbr := g[iis][jjs]
		if nbr != 9 {
			if g[i][j] < nbr && !(*seen)[iis][jjs] {
				(*seen)[iis][jjs] = true
				s += g.GetBasinSize(is[index], js[index], seen)
			}
		}
	}

	return s
}

func (g Grid) String() string {
	s := ""
	for _, row := range g {
		for _, val := range row {
			s += fmt.Sprintf("%d", val)
		}
		s += fmt.Sprintf("\n")
	}
	return s
}
