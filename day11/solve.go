package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

type Grid [10][10]int

func main() {

	if len(os.Args) != 3 {
		log.Fatal("bad args")
	}

	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	steps, _ := strconv.Atoi(os.Args[2])

	var grid Grid

	i := 0
	scan := bufio.NewScanner(f)
	for scan.Scan() {
		for j, char := range scan.Text() {
			grid[i][j] = int(char - '0')
		}
		i++
	}

	if err := scan.Err(); err != nil {
		log.Fatal(err)
	}

	fmt.Printf("step 0: \n%s\n", grid)

	flashes := 0
	for i = 0; i < steps; i++ {
		grid.IncreaseAll()
		flashes += grid.Run()
		grid.Cleanup()
		fmt.Printf("step %d: \n%s\n", i+1, grid)

		if grid.AllFlashed() {
			break
		}
	}

	fmt.Printf("answer: %d\n", flashes)
	fmt.Printf("all flashed: %d\n", i+1)
}

func (g Grid) String() string {
	s := ""
	for i, rows := range g {
		for _, val := range rows {
			if val == 0 {
				s += fmt.Sprintf("\033[1m%d\033[0m", val)
			} else {
				s += fmt.Sprintf("%d", val)
			}
		}
		if i != len(g)-1 {
			s += "\n"
		}
	}
	return s
}

func (g *Grid) IncreaseAll() {
	for i := range g {
		for j := range g[i] {
			g[i][j]++
		}
	}
}

func (g *Grid) Run() int {
	var flashed Grid
	for i := range g {
		for j := range g[0] {
			if g[i][j] > 9 {
				g.Flash(i, j, &flashed)
			}
		}
	}
	return flashed.Flashed()
}

func (g *Grid) Flash(x, y int, fd *Grid) {
	if fd[x][y] > 0 {
		return
	}
	fd[x][y] = 1
	g.IncreaseNbr(x, y, fd)
}

func (g *Grid) IncreaseNbr(x, y int, fd *Grid) {
	xs, ys := g.NbrIndexes(x, y)
	for index := range xs {
		i, j := xs[index], ys[index]
		g[i][j]++

		if g[i][j] > 9 {
			g.Flash(i, j, fd)
		}
	}
}

func (g *Grid) NbrIndexes(i, j int) ([]int, []int) {
	// find the neighbor indexes then throw away the ones that
	// are out of bounds.
	var xs, ys []int
	for _, a := range []int{-1, 0, 1} {
		for _, b := range []int{-1, 0, 1} {
			x, y := a+i, b+j
			if (x >= 0 && x < len(g[0])) && (y >= 0 && y < len(g)) {
				if x == i && y == j {
					continue
				}
				xs = append(xs, x)
				ys = append(ys, y)
			}
		}
	}
	return xs, ys
}

func (g *Grid) Flashed() int {
	total := 0
	for _, rows := range g {
		for _, val := range rows {
			if val > 0 {
				total++
			}
		}
	}
	return total
}

func (g *Grid) AllFlashed() bool {
	for _, rows := range g {
		for _, val := range rows {
			if val > 0 {
				return false
			}
		}
	}
	return true
}

func (g *Grid) Cleanup() {
	for i := range g {
		for j := range g[0] {
			if g[i][j] > 9 {
				g[i][j] = 0
			}
		}
	}
}
