package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {

	if len(os.Args) != 2 {
		log.Fatal("bad args")
	}

	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	cnt := 0

	s := bufio.NewScanner(f)
	for s.Scan() {
		tkns := strings.Split(s.Text(), " ")

		for _, t := range tkns[11:] {
			switch len(t) {
			case 2:
				cnt++
			case 3:
				cnt++
			case 4:
				cnt++
			case 7:
				cnt++
			}
		}
	}

	if err := s.Err(); err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Answer: %d\n", cnt)
}
