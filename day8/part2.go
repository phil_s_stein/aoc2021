package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strings"
)

type RuneSet map[string]bool
type Digit [10]*RuneSet

func main() {

	if len(os.Args) != 2 {
		log.Fatal("bad args")
	}

	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	s := bufio.NewScanner(f)
	for s.Scan() {

		output := [4]*Digit{NewDigit(), NewDigit(), NewDigit(), NewDigit()}
		input := [10]*Digit{
			NewDigit(), NewDigit(), NewDigit(), NewDigit(), NewDigit(),
			NewDigit(), NewDigit(), NewDigit(), NewDigit(), NewDigit(),
		}

		tkns := strings.Split(s.Text(), " ")

		for i, t := range tkns {
			if i < 10 {
				input[i].Add(t)
			} else if i == 10 {
				continue
			} else {
				output[i-11].Add(t)
			}
		}

		Solve(input, output)

		if err := s.Err(); err != nil {
			log.Fatal(err)
		}
	}

}

func Solve(input [10]*Digit, output [4]*Digit) {

	// iterate over input looking for "solved" digits.
	keyMap := make(map[string]int)
	solution := [4]int{-1, -1, -1, -1}

	for i, in := range input {
		s, val := in.Solved()
		if s != "" {
			fmt.Printf("solved input[%d]: %d == %s\n", i, val, s)
			keyMap[s] = val
		}
	}

	// write known solutions
	for i, o := range output {
		for k, val := range keyMap {
			if o.Is(k) {
				fmt.Printf("solution[%d] is %d\n", i, val)
				solution[i] = val
			}
		}
	}

	fmt.Printf("input: %s\n", input)
	// compute unknown solutions.
	for i, o := range solution {
		if o == -1 {
			if output[i].Intersect(0)==3 && output[i].Intersect...
		}
	}

	fmt.Printf("Result: %d, %d, %d, %d\n", solution[0], solution[1], solution[2], solution[3])
}

func (r *RuneSet) Intersect(rhs *RuneSet) string {
	same := ""
	for a := range *r {
		for b := range *rhs {
			if a == b {
				same += a
			}
		}
	}
	return same
}

func (d *Digit) String() string {
	o := ""
	for i, chars := range d {
		o += fmt.Sprintf("%d: %s\n", i, chars)
	}
	return o
}

func (d *Digit) Is(str string) bool {
	for _, s := range d {
		if s.String() == str {
			return true
		}
	}
	return false
}

func findSolution(input [10]*Digit, target string) int {
	return -1
}

func (d *Digit) Solved() (string, int) {
	for _, i := range []int{1, 4, 7, 8} {
		if len(*(d[i])) != 0 {
			return d[i].String(), i
		}
	}
	return "", -1
}

func NewDigit() *Digit {
	digit := &Digit{}
	for i := range digit {
		digit[i] = NewRuneSet()
	}
	return digit
}

func NewRuneSet() *RuneSet {
	r := make(RuneSet)
	return &r
}

func (r *RuneSet) String() string {
	s := []string{}
	for c := range *r {
		s = append(s, string(c))
	}
	sort.Strings(s)
	return strings.Join(s, "")
}

func (d *Digit) Add(str string) {
	switch len(str) {
	case 2: // digit is 1
		d[1].Add(str)
	case 3: // digit is 7
		d[7].Add(str)
	case 4: // digit is 4
		d[4].Add(str)
	case 5: // digit is 2, 3, or 5
		d[2].Add(str)
		d[3].Add(str)
		d[5].Add(str)
	case 6: // digit is 0, 6, or 9
		d[0].Add(str)
		d[6].Add(str)
		d[9].Add(str)
	case 7: // digit is 8
		d[8].Add(str)
	default:
		log.Fatalf("bad input len: %s", str)
	}
}

func (r RuneSet) Add(s string) {
	// no way to sort chars so sort as strings.
	tmp := []string{}

	for _, c := range []rune(s) {
		tmp = append(tmp, string(c))
	}

	sort.Strings(tmp)

	for _, c := range tmp {
		r[c] = true
	}
}
