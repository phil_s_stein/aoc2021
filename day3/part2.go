package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {

	if len(os.Args) != 2 {
		log.Fatal("bad args")
	}

	// read input into int array
	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	input := []int64{}
	sigBits := 0

	s := bufio.NewScanner(f)
	for s.Scan() {
		if sigBits == 0 {
			sigBits = len(s.Text())
		}
		i, err := strconv.ParseInt(s.Text(), 2, 64)
		if err != nil {
			log.Fatal(err)
		}
		input = append(input, i)
	}

	if err := s.Err(); err != nil {
		log.Fatal(err)
	}

	oxy := filterByCrit(sigBits, input, oxyCrit)
	scrub := filterByCrit(sigBits, input, scrubCrit)

	fmt.Printf("Answer: %d\n", oxy*scrub)
}

func filterByCrit(sigBits int, input []int64, crit func(int, []int64) int64) int64 {

	data := copyArray(input)

	for bit := sigBits - 1; bit >= 0; bit-- {

		bc := crit(bit, data)

		filtered := copyArray(data)

		for _, item := range data {
			// remove items from filtered if the bit does not match the criteria.
			if (item&(1<<bit))>>bit != bc {
				delItem(item, &filtered)
			}
		}

		data = copyArray(filtered)
		if len(data) == 1 {
			return data[0]
		}
	}

	return 0
}

func copyArray(a []int64) []int64 {
	c := make([]int64, len(a))
	copy(c, a)
	return c
}

func oxyCrit(index int, data []int64) int64 {

	ones, zeros := countOnes(index, data)
	if ones >= zeros {
		return 1
	}
	return 0
}

func scrubCrit(index int, data []int64) int64 {
	ones, zeros := countOnes(index, data)
	if zeros > ones {
		return 1
	}
	return 0
}

func countOnes(index int, data []int64) (int, int) {

	ones, zeros := 0, 0
	for _, x := range data {
		if (x&(1<<index))>>index == 1 {
			ones++
		} else {
			zeros++
		}
	}

	return ones, zeros
}

func delItem(item int64, data *[]int64) {
	for i, x := range *data {
		if x == item {
			*data = append((*data)[:i], (*data)[i+1:]...)
			return
		}
	}
}

func showBin(title string, data []int64, bitSize int) {
	fmt.Println(title)
	for _, x := range data {
		fmt.Printf("%0*b\n", bitSize, x)
	}
}
