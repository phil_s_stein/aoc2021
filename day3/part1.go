package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {

	if len(os.Args) != 2 {
		log.Fatal("bad args")
	}

	// read input into int array
	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	input := []int64{}
	sigBits := 0

	s := bufio.NewScanner(f)
	for s.Scan() {
		if sigBits == 0 {
			sigBits = len(s.Text())
		}
		i, err := strconv.ParseInt(s.Text(), 2, 64)
		if err != nil {
			log.Fatal(err)
		}
		input = append(input, i)
	}

	if err := s.Err(); err != nil {
		log.Fatal(err)
	}

	// fmt.Printf("%0*b\n", sigBits, input[0])
	count := make([]int, sigBits)

	for _, s := range input {
		for i := 0; i < sigBits; i++ {
			if (s >> i & 0x1) == 1 {
				count[i] += 1
			}
		}
	}

	fmt.Printf("count: %v\n", count)

	var gamma int
	for i, x := range count {
		if x > len(input)/2 { // should be a 1
			gamma |= 1 << i
		}
	}

	epsi := (1 << sigBits) - 1 ^ gamma // invert gamma for epsi

	fmt.Printf("gamma: %05b\n", gamma)
	fmt.Printf("epsi: %05b\n", epsi)

	fmt.Printf("Answer: %d\n", gamma*epsi)
}
