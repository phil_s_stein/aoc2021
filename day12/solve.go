package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
	"unicode"

	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/encoding/dot"
	"gonum.org/v1/gonum/graph/simple"
)

type Graph struct {
	gids      map[string]int64
	twiceSeen string
	*simple.UndirectedGraph
}

type Node struct {
	name    string
	gid     int64
	big     bool
	visited int
}

type Edge struct {
	*simple.Edge
}

func NewGraph() *Graph {
	return &Graph{
		gids:            make(map[string]int64),
		twiceSeen:       "",
		UndirectedGraph: simple.NewUndirectedGraph(),
	}
}

func NewNode(id int64, name string) *Node {
	return &Node{
		name:    name,
		gid:     id,
		visited: 0,
		big:     IsUpper(name),
	}
}

func main() {

	if len(os.Args) != 2 {
		log.Fatal("bad args")
	}

	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	g := NewGraph()
	var nid int64

	scan := bufio.NewScanner(f)
	for scan.Scan() {
		tkns := strings.Split(scan.Text(), "-")

		for _, name := range tkns {
			if _, ok := g.gids[name]; !ok {
				n := NewNode(nid, name)
				g.UndirectedGraph.AddNode(n)
				g.gids[name] = nid
				nid++
			}
		}

		a := g.NodeByName(tkns[0])
		b := g.NodeByName(tkns[1])

		e := &simple.Edge{F: a, T: b}
		g.SetEdge(e)
	}

	if err := scan.Err(); err != nil {
		log.Fatal(err)
	}

	// fmt.Printf("graph: \n%s\n", g)

	// p := path.DijkstraFrom(g.NodeByName("start"), g)
	// hops, _ := p.To(g.NodeByName("end").ID())
	// fmt.Printf("shortest %s\n", pathString(hops))

	g.Clear()

	paths := g.FindPaths(g.gids["start"], g.gids["end"])
	fmt.Printf("path count: %d\n", len(paths))
}

func (g *Graph) FindPaths(root, leaf int64) [][]graph.Node {
	g.Clear()
	paths := [][]graph.Node{}
	g.FindPath(root, leaf, nil, &paths)
	return paths
}

func (g *Graph) FindPath(root, leaf int64, path []graph.Node, allpaths *[][]graph.Node) {

	r := g.Node(root).(*Node)
	// r.visited = !r.big
	if !r.big { // big can be visited any number of times.
		if r.name == "start" || r.name == "end" {
			r.visited = 1
		} else {
			r.visited++
			if r.visited == 2 {
				g.twiceSeen = r.name
			}
		}
	}

	path = append(path, g.Node(root))

	if root == leaf {

		// last := path[len(path)-2]
		// fmt.Printf("%s (doubled: %s) (last :%s)\n", pathString(path), g.twiceSeen, last.(*Node).name)
		*allpaths = append((*allpaths), path)

		// remove last node. if this is the twice seen node, clear it.
		path = path[:len(path)-1]

	} else {

		// iterate nbrs
		nbrs := g.From(r.gid)
		for _, gn := range graph.NodesOf(nbrs) {
			n := gn.(*Node)

			if n.name == "start" {
				continue
			}
			if n.big || n.visited == 0 || (n.visited == 1 && g.twiceSeen == "") {
				g.FindPath(n.gid, leaf, path, allpaths)
			}
		}
	}

	if r.name == g.twiceSeen && r.visited == 2 {
		g.twiceSeen = ""
	}
	r.visited--
}

func (n Node) String() string {
	return fmt.Sprintf("Name: %s, Visit: %d, Big: %t", n.name, n.visited, n.big)
}

func (g *Graph) String() string {
	buf, err := dot.Marshal(g, "graph", "", "\t")
	if err != nil {
		log.Fatal(err)
	}
	return string(buf)
}

func IsUpper(s string) bool {
	for _, r := range s {
		if !unicode.IsUpper(r) && unicode.IsLetter(r) {
			return false
		}
	}
	return true
}

func (n *Node) ID() int64 {
	return n.gid
}

func (n *Node) DOTID() string {
	return n.name
}

func (g *Graph) Clear() {
	for _, n := range graph.NodesOf(g.Nodes()) {
		n.(*Node).visited = 0
	}
}

func (g *Graph) NodeByName(name string) *Node {

	id, ok := g.gids[name]
	if !ok {
		return nil
	}

	n, ok := g.UndirectedGraph.Node(id).(*Node)
	if !ok {
		return nil
	}

	return n
}

func pathString(path []graph.Node) string {

	hops := []string{}
	for _, h := range path {
		s := h.(*Node)
		hops = append(hops, s.name)
	}
	return strings.Join(hops, ",")
}

// graph stuff

// walker := &traverse.BreadthFirst{}
// walker.Visit = func(node graph.Node) {
// 	n := node.(*Node)
// 	n.visited = !n.big

// 	fmt.Printf("visited: %s\n", n)
// }

// walker.Traverse = func(e graph.Edge) bool {
// 	return true
// }

// start := g.UndirectedGraph.Node(g.gids["start"])

// until := func(n graph.Node, d int) bool {
// 	if n.ID() == g.gids["end"] {
// 		return true
// 	}
// 	return false
// }

// walker.Walk(g, start, until)

// fromA := g.From(g.gids["A"])
// fmt.Printf("Nodes from A: %s\n", pathString(graph.NodesOf(fromA)))
