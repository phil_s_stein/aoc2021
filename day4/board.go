package main

import (
	"fmt"
	"strings"
)

type Square struct {
	Value  int
	Marked bool
}

func NewSquare(value string) *Square {
	return &Square{
		Value:  str2int(value),
		Marked: false,
	}
}

func (s *Square) String() string {
	if s.Marked {
		return fmt.Sprintf("%02d* ", s.Value)
	}
	return fmt.Sprintf("%02d  ", s.Value)
}

type Board struct {
	Rows [][]*Square
	Won  bool
}

func NewBoard() *Board {
	return &Board{
		Rows: [][]*Square{},
	}
}

func (b *Board) AddRow(row string) error {

	tkns := strings.Fields(row)

	b.Rows = append(b.Rows, []*Square{})
	x := len(b.Rows) - 1

	for _, t := range tkns {
		b.Rows[x] = append(b.Rows[x], NewSquare(t))
	}

	return nil
}

func (b *Board) String() string {
	s := ""
	for _, x := range b.Rows {
		for _, y := range x {
			s += y.String()
		}
		s += "\n"
	}
	return s
}

func (b *Board) Mark(value string) {

	v := str2int(value)
	for _, x := range b.Rows {
		for _, y := range x {
			if y.Value == v {
				y.Marked = true
				return
			}
		}
	}
}

func (b *Board) UnmarkedSum() int {

	s := 0
	for _, x := range b.Rows {
		for _, y := range x {
			if !y.Marked {
				s += y.Value
			}
		}
	}

	return s
}

func (b *Board) Winner() bool {

	size := len(b.Rows[0])

	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			if b.Rows[i][j].Marked == false {
				break
			}
			if j == size-1 {
				b.Won = true
				return true // end of row all marked
			}
		}
	}

	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			if b.Rows[j][i].Marked == false {
				break
			}
			if j == size-1 {
				b.Won = true
				return true // end of col all marked
			}
		}
	}

	return false
}
