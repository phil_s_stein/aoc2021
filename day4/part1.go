package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {

	if len(os.Args) != 2 {
		log.Fatal("bad args")
	}

	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	input := []string{}
	boards := []*Board{}

	s := bufio.NewScanner(f)
	for s.Scan() {

		t := s.Text()

		if len(input) == 0 {
			input = strings.Split(t, ",")
			continue
		}

		if len(t) == 0 {
			boards = append(boards, NewBoard())
			continue
		}

		err := boards[len(boards)-1].AddRow(t)
		if err != nil {
			log.Fatal(err)
		}
	}

	if err := s.Err(); err != nil {
		log.Fatal(err)
	}

	// for _, b := range boards {
	// 	fmt.Printf("\n%s\n", b)
	// }

	for _, t := range input {
		for _, b := range boards {

			b.Mark(t)

			if b.Winner() {
				c := str2int(t)
				s := b.UnmarkedSum()
				fmt.Printf("Winner: \n%s\n", b)
				fmt.Printf("Answer: %d (%d * %d)\n", c*s, s, c)
				return
			}
		}
	}
}
