package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

var (
	part      = 1
	stepCosts = make(map[int]int)
)

func main() {

	if len(os.Args) != 3 {
		log.Fatal("bad args. want: file part")
	}

	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	input := []int{}
	part = str2int(os.Args[2])

	s := bufio.NewScanner(f)
	for s.Scan() {
		tkns := strings.Split(s.Text(), ",")

		for _, t := range tkns {
			input = append(input, str2int(t))
		}
	}

	if err := s.Err(); err != nil {
		log.Fatal(err)
	}

	frm, to := indexRange(input)

	minFuel := math.MaxInt32
	minIndex := 0

	for i := frm; i <= to; i++ {
		f := fuelCost(i, input)
		if f < minFuel {
			minFuel = f
			minIndex = i
		}
	}

	fmt.Printf("Answer: %d (at index %d)\n", minFuel, minIndex)
}

func indexRange(input []int) (int, int) {
	max := 0
	min := math.MaxInt32

	for _, v := range input {
		if v > max {
			max = v
		}
		if v < min {
			min = v
		}
	}

	return min, max
}

func fuelCost(moveTo int, input []int) int {
	cost := 0
	for _, i := range input {
		cost += stepCost(int(math.Abs(float64(i - moveTo))))
	}
	return cost
}

func stepCost(s int) int {

	if part == 1 {
		return s
	}

	if c, ok := stepCosts[s]; ok {
		return c
	}

	c := 0
	for i := 1; i <= s; i++ {
		c += i
	}

	stepCosts[s] = c

	return c
}

func str2int(str string) int {

	s, err := strconv.Atoi(str)
	if err != nil {
		log.Fatal(err)
	}

	return s
}
