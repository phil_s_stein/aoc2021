package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

var stepCosts = make(map[int]int)

func main() {

	if len(os.Args) != 2 {
		log.Fatal("bad args. want: file iterations")
	}

	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	input := []int{}

	s := bufio.NewScanner(f)
	for s.Scan() {
		tkns := strings.Split(s.Text(), ",")

		for _, t := range tkns {
			input = append(input, str2int(t))
		}
	}

	if err := s.Err(); err != nil {
		log.Fatal(err)
	}

	minFuel := math.MaxInt32
	minIndex := 0

	for i := 0; i < len(input); i++ {
		f := fuelCost(i, input)
		if f < minFuel {
			minFuel = f
			minIndex = i
		}
	}

	fmt.Printf("Answer: %d (at index %d)\n", minFuel, minIndex)
}

func fuelCost(to int, input []int) int {
	cost := 0

	for _, i := range input {
		cost += stepCost(int(math.Abs(float64(i - to))))
	}
	return cost
}

func stepCost(s int) int {

	if c, ok := stepCosts[s]; ok {
		return c
	}

	c := 0
	for i := 1; i <= s; i++ {
		c += i
	}

	stepCosts[s] = c

	return c
}

func str2int(str string) int {

	s, err := strconv.Atoi(str)
	if err != nil {
		log.Fatal(err)
	}

	return s
}
