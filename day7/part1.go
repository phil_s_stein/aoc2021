package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

func main() {

	if len(os.Args) != 2 {
		log.Fatal("bad args. want: file iterations")
	}

	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	input := []int{}

	s := bufio.NewScanner(f)
	for s.Scan() {
		tkns := strings.Split(s.Text(), ",")

		for _, t := range tkns {
			input = append(input, str2int(t))
		}
	}

	if err := s.Err(); err != nil {
		log.Fatal(err)
	}

	minFuel := math.MaxInt32
	minIndex := 0

	for i := 0; i < len(input); i++ {
		f := fuelCost(i, input)
		if f < minFuel {
			minFuel = f
			minIndex = i
		}
	}

	fmt.Printf("Answer: %d (at index %d)\n", minFuel, minIndex)
}

func fuelCost(to int, input []int) int {
	cost := 0
	for _, i := range input {
		c := int(math.Abs(float64(i - to)))
		cost += c + c
	}
	return cost
}

func str2int(str string) int {

	s, err := strconv.Atoi(str)
	if err != nil {
		log.Fatal(err)
	}

	return s
}
